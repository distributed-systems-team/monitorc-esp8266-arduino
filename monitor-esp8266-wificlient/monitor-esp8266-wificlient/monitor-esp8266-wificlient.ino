/*
    This sketch establishes a TCP connection to a "quote of the day" service.
    It sends a "hello" message, and then prints received data.
*/

#include <ESP8266WiFi.h>
#include <DHT_U.h>
#include <DHT.h>
#include <time.h>

// D1: digital PIN sensor | DHT11: type sensor
DHT dht(D1, DHT11);

const char *host = "192.168.100.4";
const uint16_t port = 3000;
String ID_DEVICE;
// Use WiFiClient class to create TCP connections
WiFiClient client;

void setup()
{
  Serial.begin(115200);
  connectWiFiWPS();
  if (WiFi.status() == WL_CONNECTED)
  {
    // initialize dht11 sensor
    dht.begin();
    delay(500);
  }
  else
  {
    Serial.print("Don't connect WiFi by WPS");
  }
  Serial.print("WiFi connected IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println(WiFi.macAddress());
  // initialize date time process
  configTime(-4 * 3600, 0, "pool.ntp.org", "time.nist.gov");
  Serial.println("\nWaiting for time");
  while (!time(nullptr))
  {
    Serial.print(".");
    delay(1000);
  }
  Serial.println("");
}

void loop()
{
  if (WiFi.status() == WL_CONNECTED)
  {
    if (!client.connected())
    {
      Serial.print("connecting to ");
      Serial.print(host);
      Serial.print(port);
      if (!client.connect(host, port))
      {
        Serial.println("connection failed");
        delay(5000);
        return;
      }
    }

    // read info sensor obtained
    float temperatura = dht.readTemperature();
    float humedad = dht.readHumidity();
    float sensacionTermica = dht.computeHeatIndex(temperatura, humedad);

    // obtained date time current
    time_t now = time(nullptr);
    String dateTimeCurrent = ctime(&now);

    // This will send a string to the server
    if (client.connected())
    {
      Serial.println("sending data to server: " + ID_DEVICE + "," + String(temperatura, 3) + "," + String(humedad, 3) + "," + String(sensacionTermica, 3) + "," + dateTimeCurrent);
      client.println("to:[*]#data:" + ID_DEVICE + "," + String(temperatura, 3) + "," + String(humedad, 3) + "," + String(sensacionTermica, 3) + "," + dateTimeCurrent);
    }
    delay(5000); // execute once every 5 seconds, don't flood remote service
  }
  else
  {
    reconnectWiFiWPS();
  }
}

void connectWiFiWPS()
{
  Serial.printf("WiFi Mode established a WIFI_STATUS %s\n", WiFi.mode(WIFI_STA) ? "" : "Failed!");
  Serial.print("Begin WPS (press button WPS its router) ... ");
  Serial.println(WiFi.beginWPSConfig() ? "OK" : "Failed!");
  WiFi.setAutoConnect(true);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print("connecting...");
  }
  Serial.print("Connected, IP_ADDR: ");
  Serial.println(WiFi.localIP());
  ID_DEVICE = WiFi.macAddress();
}

void reconnectWiFiWPS()
{
  WiFi.reconnect();
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print("reconnecting....");
  }
  Serial.println("Reconnect, IP address: ");
  Serial.println(WiFi.localIP());
  ID_DEVICE = WiFi.macAddress();
}
