#include <DHT_U.h>
#include <DHT.h>
#include <ESP8266WiFi.h>

#ifndef STASSID
#define STASSID "Flia Quispe"
#define STAPSK  "fliaQuispe#3$u3$"
#endif


// variables globales
// D1: pin digital donde conectaremos el sensor
// DHT11: tipo de sensor a usar
DHT dht(D1, DHT11);

float temperatura,temperaturaFarenheith, humedad, sensacionTermica;
const char* ssid     = STASSID;
const char* password = STAPSK;

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
  // initialize dht sensor
  dht.begin();
  delay(500);
  
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  /* Explicitly set the ESP8266 to be a WiFi-client, otherwise, it by default,
     would try to act as both a client and an access-point and could cause
     network-issues with your other WiFi-devices on your WiFi-network. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  // connect to WiFi
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// the loop function runs over and over again forever
void loop() {
  temperatura = dht.readTemperature();
  temperaturaFarenheith = dht.readTemperature(true);
  humedad = dht.readHumidity();
  
  // Check if any reads failed and exit early (to try again).
  if (isnan(humedad) || isnan(temperatura) || isnan(temperaturaFarenheith) || isnan(sensacionTermica)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
  
  sensacionTermica = dht.computeHeatIndex(temperatura, humedad, false);
  // sensacion termica
  
  Serial.println("Temperatura °C: "+String(temperatura)+" Temperatura °F: "+String(temperaturaFarenheith)+" Humedad: "+String(humedad)+" sensacion Termica: "+sensacionTermica);
  digitalWrite(LED_BUILTIN, HIGH);    // turn the LED off by making the voltage LOW
  delay(5000);
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
}
