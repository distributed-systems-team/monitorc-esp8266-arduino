#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include <WebSocketsClient_Generic.h> 
#include <SocketIOclient_Generic.h>
#include <Hash.h> 
#include <DHT_U.h>
#include <DHT.h>

// attr socketIO client
SocketIOclient socketIO;

// Select the IP address according to your local network
IPAddress serverIP(192, 168, 100, 47);
uint16_t  serverPort = 3000;

// D1: digital PIN sensor | DHT11: type sensor
DHT dht(D1, DHT11);

// variable info
float temperatura, humedad, sensacionTermica;

void setup() {
  // listen input USB port
  Serial.begin(115200);
  connectWiFiWPS();
  if (WiFi.status() == WL_CONNECTED) {
    Serial.print("Start ESP8266_WebSocketClientSocketIO on "); 
    Serial.println(ARDUINO_BOARD);
    Serial.println(WEBSOCKETS_GENERIC_VERSION);

    // server address, port and URL
    Serial.print("Connecting to WebSockets Server IP address: ");
    Serial.print(serverIP);
    Serial.print(", port: ");
    Serial.println(serverPort);

    // initialize socketIO client
    socketIO.begin(serverIP, serverPort);

    // initialize dht11 sensor
    dht.begin();
    delay(500);

    // event handler
    socketIO.onEvent(socketIOEvent);
  }else{
    Serial.print("Don't connect WiFi by WPS");
  }
}

void connectWiFiWPS(){
  Serial.printf("Modo WiFi establecido a WIFI_STA %s\n", WiFi.mode(WIFI_STA) ? "" : "Falló!");
  Serial.print("Comienzo WPS (presione el botón WPS en su router) ... ");
  Serial.println(WiFi.beginWPSConfig() ? "OK" : "Falló!");
  WiFi.setAutoConnect(true);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print("connecting...");
  }
  Serial.println();
  Serial.print("Conectado, dirección IP: ");
  Serial.println(WiFi.localIP());
}

void reconnectWiFiWPS(){
  WiFi.reconnect();
  while (WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print("reconnecting....");
  }
  Serial.println();
  Serial.print("Reconectado, dirección IP: ");
  Serial.println(WiFi.localIP());
}
-----------------------------------------------------------------------------------------------

unsigned long messageTimestamp = 0;

void loop() {
  if (WiFi.status() == WL_CONNECTED){
    socketIO.loop();
    uint64_t now = millis();

    if (now - messageTimestamp > 5000) {
      messageTimestamp = now;

      // creat JSON message
      DynamicJsonDocument doc(2048);
      JsonArray array = doc.to<JsonArray>();
    
      // add event name ( Ex. Hint: socket.on('event_name', ....) )
      array.add("device-temperature");
      // read info sensor obtained
      temperatura = dht.readTemperature();
      humedad = dht.readHumidity();
      sensacionTermica = dht.computeHeatIndex(temperatura, humedad);
    
      // add payload (parameters) for the event
      JsonObject param1 = array.createNestedObject();
      param1["temperatura"] = temperatura;
      param1["humedad"] = humedad;
      param1["sensaciontermica"] = sensacionTermica;
    
      // JSON to String (serializing)
      String output;
      serializeJson(doc, output);
    
      // Send event
      socketIO.sendEVENT(output);
    
      // Print JSON for debugging
      Serial.println(output);
    }
  }else{
    reconnectWiFiWPS();
  }
}
//------------------------------------------------------------------------

void socketIOEvent(const socketIOmessageType_t& type, uint8_t * payload, const size_t& length)
{
  switch (type)
  {
    case sIOtype_DISCONNECT:
      Serial.println("[IOc] Disconnected");
      break;
    case sIOtype_CONNECT:
      Serial.print("[IOc] Connected to url: ");
      Serial.println((char*) payload);
      // join default namespace (no auto join in Socket.IO V3)
      socketIO.send(sIOtype_CONNECT, "/");
      break;
    case sIOtype_EVENT:
      Serial.print("[IOc] Get event: ");
      Serial.println((char*) payload);
      break;
    case sIOtype_ACK:
      Serial.print("[IOc] Get ack: ");
      Serial.println(length);
      hexdump(payload, length);
      break;
    case sIOtype_ERROR:
      Serial.print("[IOc] Get error: ");
      Serial.println(length);
      hexdump(payload, length);
      break;
    case sIOtype_BINARY_EVENT:
      Serial.print("[IOc] Get binary: ");
      Serial.println(length);
      hexdump(payload, length);
      break;
    case sIOtype_BINARY_ACK:
      Serial.print("[IOc] Get binary ack: ");
      Serial.println(length);
      hexdump(payload, length);
      break;
    case sIOtype_PING:
      Serial.println("[IOc] Get PING");
      break;
    case sIOtype_PONG:
      Serial.println("[IOc] Get PONG");
      break;
    default:
      break;
  }
}
